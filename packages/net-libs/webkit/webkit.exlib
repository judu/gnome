# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ ninja=true ]
require utf8-locale
require gsettings
require flag-o-matic
require toolchain-funcs

export_exlib_phases pkg_setup src_prepare src_install

myexparam required_gcc=7.3
myexparam -b use_gtk4=false
myexparam -b use_soup2=false

if exparam -b use_gtk4; then
    # Note that api 5.0 is an "experimental" version of the gtk webkit api, but it was released and
    # is required by some GNOME 43 components, so we make it available.
    # Future webkitgtk versions (probably >=2.40) will use api 6.0 instead.
    SLOT="5.0"
elif exparam -b use_soup2; then
    SLOT="4.0"
else
    SLOT="4.1"
fi

SUMMARY="GTK Web Browser Engine based on WebKit"
HOMEPAGE="https://webkitgtk.org"

DOWNLOADS="https://webkitgtk.org/releases/webkitgtk-${PV}.tar.xz"

LICENCES="LGPL-2 LGPL-2.1 BSD-3"

MYOPTIONS="
    avif [[ description = [ Support for AVIF images ] ]]
    gobject-introspection
    gtk-doc [[ requires = gobject-introspection ]]
    hyphen [[ description = [ Enable hyphenation using libhyphen ] ]]
    journald [[ description = [ Support sending logging output directly to systemd-journald ] ]]
    jpeg2000 [[ description = [ Support for the JPEG2000 images ] ]]
    jpegxl [[ description = [ Support for JPEG-XL images ] ]]
    lcms
    opengl [[ description = [ Enable OpenGL & WebGL support ] ]]
    spell
    X
    wayland [[ requires = opengl ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: ar as bg cs de el en_CA en_GB eo es et eu fr gl gu he hi hu id it kn ko lt lv ml mr
               nb nl or pa pl pt pt_BR ro ru sl sr sr@latin sv ta te uk vi zh_CN )
    ( platform: amd64 )
"

RESTRICT="test" # requires X

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.10.0]
        dev-lang/python:*[>=2.7.0]
        dev-lang/ruby:*[>=1.9]
        dev-util/gperf[>=3.0.1]
        sys-devel/gettext
        sys-devel/ninja
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.32.0] )
        gtk-doc? ( dev-doc/gi-docgen )
    build+run:
        dev-db/sqlite:3[>=3.0]
        dev-libs/at-spi2-core[>=2.6.2][X?]
        dev-libs/glib:2[>=2.56.4]
        dev-libs/icu:=[>=60.2]
        dev-libs/libgcrypt[>=1.7.0]
        dev-libs/libsecret:1
        dev-libs/libtasn1
        dev-libs/libxml2:2.0[>=2.8.0]
        dev-libs/libxslt[>=1.1.7]
        gnome-desktop/libgudev
        media-libs/fontconfig[>=2.8.0]
        media-libs/freetype:2[>=2.4.2]
        media-libs/gstreamer:1.0[>=1.10.0]
        media-libs/libpng:=[>=1.2]
        media-libs/libwebp:=
        media-libs/woff2[>=1.0.2]
        media-plugins/gst-plugins-base:1.0[>=1.10.0][X?][wayland?]
        media-plugins/gst-plugins-bad:1.0[>=1.10.0]
        sys-apps/bubblewrap
        sys-apps/xdg-dbus-proxy
        sys-libs/libseccomp
        sys-libs/zlib
        x11-dri/mesa[X?][wayland?]
        x11-libs/cairo[>=1.16][X?]
        x11-libs/harfbuzz[>=0.9.18]
        x11-libs/pango[>=1.32.0]
        avif? ( media-libs/libavif:=[>=0.9.0] )
        hyphen? ( office-libs/hyphen )
        journald? ( sys-apps/systemd )
        jpeg2000? ( media-libs/OpenJPEG:2[>=2.2.0] )
        jpegxl? ( media-libs/libjxl )
        lcms? ( media-libs/lcms2 )
        opengl? (
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:opengl]
        )
        spell? ( app-spell/enchant:2 )
        X? (
            x11-libs/libXcomposite
            x11-libs/libXdamage
            x11-libs/libXrender
            x11-libs/libXt
        )
        wayland? (
            dev-libs/libwpe:1.0[>=1.3.0]
            dev-libs/wpebackend-fdo:1.0[>=1.6.0]
            sys-libs/wayland
            sys-libs/wayland-protocols[>=1.12]
        )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    recommendation:
        gps/geoclue:2.0 [[ description = [ Required for geolocation support ] ]]
"

if exparam -b use_gtk4; then
    DEPENDENCIES+="
        build+run:
            x11-libs/gtk:4.0[>=3.98.5][X?][wayland?]
    "
else
    DEPENDENCIES+="
        build+run:
            x11-libs/gtk+:3[>=3.22.0][gobject-introspection?][X?][wayland?]
    "
fi

if exparam -b use_soup2; then
    DEPENDENCIES+="
        build+run:
            gnome-desktop/libsoup:2.4[>=2.54.0][gobject-introspection?]
    "
else
    DEPENDENCIES+="
        build+run:
            gnome-desktop/libsoup:3.0[>=3.0.0][gobject-introspection?]
    "
fi

CMAKE_SOURCE="${WORKBASE}/webkitgtk-${PV}/"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # cmake script gets this wrong
    -DGI_GIRDIR:PATH=/usr/share/gir-1.0
    -DGI_TYPELIBDIR:PATH=/usr/$(exhost --target)/lib/girepository-1.0

    -DPORT=GTK

    -DUSE_GTK4=$(exparam -b use_gtk4 && echo "ON" || echo "OFF")
    -DUSE_SOUP2=$(exparam -b use_soup2 && echo "ON" || echo "OFF")

    # Needs unpackaged libmanette
    -DENABLE_GAMEPAD:BOOL=OFF

    # Installs /usr/${host}/bin/WebKitWebDriver which collides between slots
    -DENABLE_WEBDRIVER:BOOL=OFF
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection INTROSPECTION'
    'gtk-doc DOCUMENTATION'
    'journald JOURNALD_LOG'
    'spell SPELLCHECK'
    'X X11_TARGET'
    'wayland WAYLAND_TARGET'
)
CMAKE_SRC_CONFIGURE_OPTION_USES=(
    'avif AVIF'
    'hyphen LIBHYPHEN'
    'jpeg2000 OPENJPEG'
    'jpegxl JPEGXL'
    'lcms LCMS'
    'opengl OPENGL_OR_ES'
)

webkit_pkg_setup() {
    # Required to prevent UnicodeDecodeError when executing `cssmin.py` script
    require_utf8_locale
}

webkit_src_prepare() {
    local required_gcc

    exparam -v required_gcc required_gcc
    if cc-is-gcc ; then
        ever at_least ${required_gcc} $(gcc-version) ||
            die "GCC[>=${required_gcc}] required to build ${PN} (use \`eclectic gcc' to change the active compiler)"
    fi

    # NOTE(compnerd) tweak compilation options for memory
    # This is particularly important after 1.10 as the build has gotten to the point where it
    # will start hitting 4GB limitations on 32-bit platforms otherwise

    # reducing the debug symbols to minimum makes a *significant* impact
    # ar has started hitting 4G limitations in certain cases
    # https://bugs.webkit.org/show_bug.cgi?id=91154
    [[ " ${CXXFLAGS} " == *\ -g* ]] && append-flags -g1

    # https://bugs.webkit.org/show_bug.cgi?id=135936
    # build fails if optimizing but NDEBUG undefined
    [[ " ${CXXFLAGS} " == *\ -O* && " ${CXXFLAGS} " != *\ -O0* ]] && append-flags -DNDEBUG

    # attempt to reduce memory usage during the linking phase
    append-ldflags -Wl,--no-keep-memory
    [[ $(eclectic ld show) == bfd ]] && append-ldflags -Wl,--reduce-memory-overheads

    # Webkit doesn't work with musl's default (small) thread stack sizes.
    # https://bugs.webkit.org/show_bug.cgi?id=187485
    # Set the stack size on musl to match glibc's x86 default 2MB
    [[ $(exhost --target) == *-musl* ]] && append-ldflags -Wl,-z,stack-size=2097152

    cmake_src_prepare
}

webkit_src_install() {
    cmake_src_install

    edo mv "${IMAGE}"/usr/{$(exhost --target)/,}share/locale
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
}

