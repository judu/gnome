# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings gtk-icon-cache
require freedesktop-desktop freedesktop-mime
require meson

SUMMARY="File Manager for GNOME"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    extensions [[ description = [ Build image and audio/video property pages and sendto plugin ] ]]
    gtk-doc
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.4] )
    build+run:
        app-pim/tracker:3.0
        app-pim/tracker-miners:3.0
        dev-libs/appstream-glib
        dev-libs/atk
        dev-libs/glib:2[>=2.67.1]
        dev-libs/libhandy:1[>=1.5.0]
        dev-libs/libxml2:2.0[>=2.7.8]
        dev-util/desktop-file-utils
        gnome-desktop/gnome-autoar[>=0.4.0]
        gnome-desktop/gnome-desktop:4[>=3.0.0][legacy]
        gnome-desktop/gobject-introspection:1[>=0.6.4]
        gnome-desktop/gsettings-desktop-schemas[>=42]
        sys-libs/libseccomp
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.22.27][gobject-introspection]
        x11-libs/libnotify[>=0.7]
        x11-libs/pango[>=1.28.3]
        extensions? (
            dev-libs/gexiv2[>=0.14.0]
            media-plugins/gst-plugins-base:1.0
        )
    run:
        gnome-desktop/gvfs
    recommendation:
        gnome-desktop/gnome-disk-utility [[ note = [ required for disk management ] ]]
    suggestion:
        gnome-desktop/adwaita-icon-theme[>=1.1.91]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dintrospection=true
    -Dlibportal=false
    -Dpackagekit=false
    -Dprofiling=false
    -Dselinux=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'extensions'
    'gtk-doc docs'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=headless -Dtests=none'
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

