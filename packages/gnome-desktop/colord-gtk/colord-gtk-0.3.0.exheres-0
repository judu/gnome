# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require meson \
    vala [ with_opt=true option_name=vapi vala_dep=true  ]

SUMMARY="GTK+ support library for colord"
HOMEPAGE="https://www.freedesktop.org/software/colord"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.xz"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    vapi
    (
        providers:
            gtk3 [[ description = [ Build colord-gtk ] ]]
            gtk4 [[ description = [ Build colord-gtk4 ] ]]
    )
"

# requires X
RESTRICT="test"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-ns-stylesheets
        dev-libs/libxslt
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        dev-libs/glib:2[>=2.28.0]
        gnome-desktop/gobject-introspection:1[>=0.9.8]
        sys-apps/colord[>=1.4.1][vapi?]
        providers:gtk3? ( x11-libs/gtk+:3[gobject-introspection] )
        providers:gtk4? ( x11-libs/gtk:4.0[>=4.4] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgtk2=false
    -Dintrospection=true
    -Dman=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc docs'
    'providers:gtk3'
    'providers:gtk4'
    vapi
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

