# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gcr

LICENCES="GPL-2"
SLOT="4"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc [[ requires = gobject-introspection ]]
    vapi [[ requires = gobject-introspection ]]
    systemd
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.4]
        sys-devel/gettext
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.34.0] )
        gtk-doc? ( dev-doc/gi-docgen[>=1.9] )
    build+run:
        app-crypt/gnupg
        dev-libs/glib:2[>=2.68.0]
        dev-libs/libgcrypt[>=1.4.5]
        dev-libs/libsecret:1[>=0.20]
        dev-libs/p11-kit:1[>=0.19.0]
        sys-apps/dbus
        x11-libs/gtk:4.0
        systemd? ( sys-apps/systemd )
        !gnome-desktop/gnome-keyring:1[<3.3] [[
            description = [ gcr is now in a separate package, was part of gnome-keyring before ]
            resolution = uninstall-blocked-after
        ]]
        !gnome-desktop/gcr:0[<3.41.1-r1] [[
            description = [ gcr:0 used to install gcr-ssh-agent which is now ship with gcr:4 ]
            resolution = upgrade-blocked-before
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dgtk4=true'
    '-Dssh_agent=true'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'vapi'
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    systemd
)

src_prepare() {
    meson_src_prepare

    # These gcr-ssh-agent tests time out with sydbox (1.2.1)
    edo sed -e "/'process'/d" \
            -e "/'service'/d" \
            -i gcr/meson.build
}

