# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="Desktop environment for GNOME"
HOMEPAGE="http://www.gnome.org/"

LICENCES="GPL-2"
SLOT="4"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc
    legacy [[ description = [ Enable support for legacy gnome-desktop:3.0 compat ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.18]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.53.0]
        gnome-desktop/gobject-introspection:1[>=0.9.7]
        gnome-desktop/gsettings-desktop-schemas[>=3.27.0][gobject-introspection]
        sys-apps/bubblewrap
        sys-libs/libseccomp
        x11-apps/xkeyboard-config
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.36.5][gobject-introspection]
        x11-libs/gtk:4.0[>=4.4.0]
        x11-libs/libxkbcommon[>=1.1]
        legacy? ( x11-libs/gtk+:3[>=3.3.6][gobject-introspection] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        !gnome-desktop/gnome-desktop:3.0 [[
            description = [ Replaced by gnome-destop:4[legacy] ]
            resolution = uninstall-blocked-before
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-thumbnail-Bind-mount-Exherbo-alternatives-path-to-sa.patch
)

MESON_SOURCE="${WORKBASE}/${PNV/_/.}"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgnome_distributor=Exherbo
    -Ddesktop_docs=false
    -Dinstalled_tests=false
    -Dudev=enabled
    -Dbuild_gtk4=true
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=( 'providers:systemd' )
MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'gtk-doc gtk_doc' 'legacy legacy_library' )

# Tries to connect to wayland socket
RESTRICT="test"

src_test() {
    unset DISPLAY
    meson_src_tests
}

