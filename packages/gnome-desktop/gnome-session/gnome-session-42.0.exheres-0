# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome-session
require option-renames [ renames=[ 'systemd providers:systemd' ] ]

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc
    man-pages [[ description = [ generate and install man-pages ] ]]
    ( linguas: af am an ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs csb cy da
               de dz el en_CA en_GB en@shaw eo es et eu fa fi fr fur fy ga gl gu ha he hi hr hu hy
               id ig is it ja ka kk km kn ko ku lt lv mai mg mi mk ml mn mr ms nb nds ne nl nn nso
               oc or pa pl ps pt pt_BR ro ru rw si sk sl sq sr sr@latin sv ta te tg th tk tr ug uk
               uz uz@cyrillic vi wa xh yo zh_CN zh_HK zh_TW zu )
    ( providers: consolekit elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        virtual/pkg-config[>=0.20]
        doc? ( app-text/xmlto )
    build+run:
        core/json-glib[>=0.10]
        dev-libs/glib:2[>=2.46.0]
        dev-libs/libepoxy
        dev-libs/libglvnd [[ note = [ provides {egl,gl,glesv2}.pc ] ]]
        gnome-desktop/gnome-desktop:4[>=3.34.2][legacy]
        x11-libs/gtk+:3[>=3.22.0]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/xtrans
        providers:consolekit? ( dev-libs/dbus-glib:1[>=0.76] )
        providers:elogind? ( sys-auth/elogind[>=209] )
        providers:systemd? ( sys-apps/systemd[>=209][polkit] )
    run:
        gnome-desktop/gsettings-desktop-schemas[>=0.1.7]
        (
            gnome-desktop/gnome-shell
            gnome-desktop/gnome-settings-daemon
        ) [[ *description = [ minimum requirements for the default session ] ]]
    recommendation:
        gnome-desktop/zenity[>=3.7.0] [[ description = [ used to display init errors ] ]]
        providers:consolekit? ( sys-auth/ConsoleKit2 ) [[ note = [ elogind/systemd provide session management ] ]]
    suggestion:
        gnome-desktop/polkit-gnome:1
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-meson-allow-usage-of-elogind.patch
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc docbook'
    'man-pages man'
    'providers:consolekit consolekit'
    'providers:elogind elogind'
    'providers:systemd systemd'
    'providers:systemd systemd_journal'
    'providers:systemd systemd_session default disable'
)

src_prepare() {
    meson_src_prepare
    edo sed -e "/systemd_userunitdir/s/session_prefix/'\/usr\/$(exhost --target)'/" -i data/meson.build
}

