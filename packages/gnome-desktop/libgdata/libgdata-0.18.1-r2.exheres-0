# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ with_opt=true vala_dep=true ]
require meson

SUMMARY="GLib-based library for accessing online service APIs using the GData protocol"
DESCRIPTION="
libgdata is a GLib-based library for accessing online service APIs using the
GData protocol --- most notably, Google's services. It provides APIs to access
the common Google services, and has full asynchronous support.
"
HOMEPAGE="https://wiki.gnome.org/Projects/libgdata"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gnome [[ description = [ Support for transparent proxy and non-pageable memory through GNOME ] ]]
    gobject-introspection
    gtk-doc
    oauth [[ description = [ Enable deprecated OAuth 1.0 support ] ]]
    online-accounts [[ requires = gnome ]]
    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.7] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.25] )
    build+run:
        core/json-glib[>=0.15]
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libxml2:2.0
        gnome-desktop/libsoup:2.4[>=2.42.0][gobject-introspection?]
        x11-libs/gtk+:3[>=2.91.2]
        gnome? ( gnome-desktop/gcr:0 )
        oauth? ( dev-libs/liboauth[>=0.9.4] )
        online-accounts? ( gnome-desktop/gnome-online-accounts[>=3.8][gobject-introspection?][vapi?] )
    build+test:
        dev-libs/uhttpmock[>=0.5.0]
        x11-libs/gdk-pixbuf:2.0[>=2.14] [[ note = [ automagic dependency ] ]]
"

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gnome'
    'oauth oauth1'
    'online-accounts goa'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'vapi'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dalways_build_tests=true -Dalways_build_tests=false'
)

