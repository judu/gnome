# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache
require option-renames [ renames=[ 'modem-manager wwan' ] ]
require meson

SUMMARY="A panel applet to configure networking via NetworkManager"
HOMEPAGE="http://www.gnome.org/projects/NetworkManager"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    appindicator [[ description = [ build in support for libappindicator besides Xembed ] ]]
    wwan [[ description = [ support management of WWAN via Modem Manager ] ]]
    ( linguas: af an ar as ast be be@latin bg bn_IN bs ca ca@valencia crh cs da de dz el en_CA en_GB
               eo es et eu fa fi fr gd gl gu he hi hr hu id it ja kk km kn ko ku lt lv mk ml mr ms nb
               ne nl nn oc or pa pl pt pt_BR ro ru rw sk sl sq sr sr@latin sv ta te tg th tr ug uk ur
               vi wa zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18]
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.38]
        dev-libs/libsecret:1[>=0.18]
        gnome-desktop/libgudev[>=147]
        net-apps/NetworkManager[>=1.16]
        net-libs/libnma[>=1.8.28]
        x11-libs/gtk+:3[>=3.10]
        x11-libs/libnotify[>=0.7]
        appindicator? ( dev-libs/libappindicator:= )
        wwan? ( net-wireless/ModemManager )
    run:
        sys-apps/dbus[>=1.2.6]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dselinux=false
    -Dteam=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'appindicator appindicator yes no'
    'wwan'
)

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

