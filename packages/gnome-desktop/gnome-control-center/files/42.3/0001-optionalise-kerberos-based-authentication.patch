From 1fa3dde790aa6cde07a5556665f4dc3cab599875 Mon Sep 17 00:00:00 2001
From: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
Date: Thu, 8 Apr 2021 15:31:23 +0200
Subject: [PATCH 1/2] optionalise kerberos based authentication

Signed-off-by: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
---
 meson.build                             | 11 +++++++++++
 meson_options.txt                       |  1 +
 panels/user-accounts/cc-realm-manager.c |  9 +++++++++
 panels/user-accounts/cc-realm-manager.h |  1 +
 panels/user-accounts/meson.build        |  8 +++++---
 5 files changed, 27 insertions(+), 3 deletions(-)

diff --git a/meson.build b/meson.build
index 09aa30f84..f4c46c1af 100644
--- a/meson.build
+++ b/meson.build
@@ -184,6 +184,16 @@ endif
 config_h.set('HAVE_IBUS', enable_ibus,
              description: 'Defined if IBus support is enabled')
 
+# Kerberos support
+enable_kerberos = get_option('kerberos')
+if enable_kerberos
+    kerberos_deps = [
+      dependency('krb5')
+    ]
+endif
+config_h.set('HAVE_KERBEROS', enable_kerberos,
+             description: 'Defined if kerberos support is enabled')
+
 # thunderbolt
 config_h.set10('HAVE_FN_EXPLICIT_BZERO',
                cc.has_function('explicit_bzero', prefix: '''#include <string.h>'''),
@@ -305,6 +315,7 @@ summary({
 
 summary({
   'IBus': enable_ibus,
+  'Kerberos': enable_kerberos,
   'Snap': enable_snap,
   'Malcontent': enable_malcontent,
 }, section: 'Optional Dependencies')
diff --git a/meson_options.txt b/meson_options.txt
index dbca72387..bd5570fb4 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -1,5 +1,6 @@
 option('documentation', type: 'boolean', value: false, description: 'build documentation')
 option('ibus', type: 'boolean', value: true, description: 'build with IBus support')
+option('kerberos', type: 'boolean', value: true, description: 'build with kerberos support')
 option('privileged_group', type: 'string', value: 'wheel', description: 'name of group that has elevated permissions')
 option('snap', type: 'boolean', value: false, description: 'build with Snap support')
 option('tests', type: 'boolean', value: true, description: 'build tests')
diff --git a/panels/user-accounts/cc-realm-manager.c b/panels/user-accounts/cc-realm-manager.c
index bc43e6d0f..7872480df 100644
--- a/panels/user-accounts/cc-realm-manager.c
+++ b/panels/user-accounts/cc-realm-manager.c
@@ -22,7 +22,9 @@
 
 #include "cc-realm-manager.h"
 
+#if defined(HAVE_KERBEROS)
 #include <krb5/krb5.h>
+#endif
 
 #include <glib.h>
 #include <glib/gi18n.h>
@@ -596,6 +598,7 @@ login_closure_free (gpointer data)
         g_slice_free (LoginClosure, login);
 }
 
+#if defined(HAVE_KERBEROS)
 static krb5_error_code
 login_perform_kinit (krb5_context k5,
                      const gchar *realm,
@@ -741,6 +744,7 @@ kinit_thread_func (GTask *t,
         if (k5)
                 krb5_free_context (k5);
 }
+#endif
 
 void
 cc_realm_login (CcRealmObject *realm,
@@ -773,7 +777,12 @@ cc_realm_login (CcRealmObject *realm,
         g_task_set_task_data (task, login, login_closure_free);
 
         g_task_set_return_on_cancel (task, TRUE);
+#if defined(HAVE_KERBEROS)
         g_task_run_in_thread (task, kinit_thread_func);
+#else
+        g_task_return_new_error (task, CC_REALM_ERROR, CC_REALM_ERROR_NOT_SUPPORTED,
+                                 _("kerberos based authentication support is disabled"));
+#endif
 }
 
 GBytes *
diff --git a/panels/user-accounts/cc-realm-manager.h b/panels/user-accounts/cc-realm-manager.h
index 7e68e8e37..77910af26 100644
--- a/panels/user-accounts/cc-realm-manager.h
+++ b/panels/user-accounts/cc-realm-manager.h
@@ -29,6 +29,7 @@ typedef enum {
         CC_REALM_ERROR_BAD_PASSWORD,
         CC_REALM_ERROR_CANNOT_AUTH,
         CC_REALM_ERROR_GENERIC,
+        CC_REALM_ERROR_NOT_SUPPORTED,
 } CcRealmErrors;
 
 #define CC_REALM_ERROR (cc_realm_error_get_quark ())
diff --git a/panels/user-accounts/meson.build b/panels/user-accounts/meson.build
index d9efd1fd8..f5e9d64e3 100644
--- a/panels/user-accounts/meson.build
+++ b/panels/user-accounts/meson.build
@@ -164,15 +164,13 @@ sources += gnome.mkenums_simple(
   sources: files(enum_headers))
 
 # Kerberos support
-krb_dep = dependency('krb5', required: false)
-assert(krb_dep.found(), 'kerberos libraries not found in your path')
+krb_dep = dependency('krb5', required: enable_kerberos)
 
 deps = common_deps + [
   accounts_dep,
   gdk_pixbuf_dep,
   gnome_desktop_dep,
   liblanguage_dep,
-  krb_dep,
   m_dep,
   polkit_gobject_dep,
   pwquality_dep,
@@ -182,6 +180,10 @@ if enable_malcontent
   deps += malcontent_dep
 endif
 
+if enable_kerberos
+  deps += krb_dep
+endif
+
 cflags += [
   '-DGNOMELOCALEDIR="@0@"'.format(control_center_localedir),
   '-DHAVE_LIBPWQUALITY',
-- 
2.35.1

