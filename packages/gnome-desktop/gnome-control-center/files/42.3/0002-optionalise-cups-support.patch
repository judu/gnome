From ce263053b4d1c162ff753041367d9f7db1257d16 Mon Sep 17 00:00:00 2001
From: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
Date: Sun, 20 Mar 2022 10:07:03 +0100
Subject: [PATCH 2/2] optionalise cups support

Signed-off-by: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
---
 meson.build                | 49 ++++++++++++++++++++++----------------
 meson_options.txt          |  1 +
 panels/meson.build         |  5 +++-
 shell/cc-panel-loader.c    |  4 ++++
 tests/printers/meson.build | 38 ++++++++++++++---------------
 5 files changed, 56 insertions(+), 41 deletions(-)

diff --git a/meson.build b/meson.build
index f4c46c1af..2dbcccaaf 100644
--- a/meson.build
+++ b/meson.build
@@ -152,29 +152,35 @@ foreach polkit_file: polkit_files
   assert(r.returncode() == 0, 'ITS support missing from polkit, please upgrade or contact your distribution')
 endforeach
 
-# Check for CUPS 1.4 or newer
-cups_dep = dependency('cups', version : '>= 1.4', required: false)
-assert(cups_dep.found(), 'CUPS 1.4 or newer not found')
-
-# https://bugzilla.gnome.org/show_bug.cgi?id=696766
-cups_cflags = []
-if cups_dep.version().version_compare('>= 1.6')
-  cups_cflags += '-D_PPD_DEPRECATED='
-endif
-
-# cups headers
-check_headers = [
-  ['HAVE_CUPS_CUPS_H', 'cups/cups.h'],
-  ['HAVE_CUPS_PPD_H', 'cups/ppd.h']
-]
+# Optional dependency for the printers panel
+enable_cups = get_option('cups')
+if enable_cups
+  # Check for CUPS 1.4 or newer
+  cups_dep = dependency('cups', version : '>= 1.4', required: false)
+  assert(cups_dep.found(), 'CUPS 1.4 or newer not found')
+
+  # https://bugzilla.gnome.org/show_bug.cgi?id=696766
+  cups_cflags = []
+  if cups_dep.version().version_compare('>= 1.6')
+    cups_cflags += '-D_PPD_DEPRECATED='
+  endif
+
+  # cups headers
+  check_headers = [
+    ['HAVE_CUPS_CUPS_H', 'cups/cups.h'],
+    ['HAVE_CUPS_PPD_H', 'cups/ppd.h']
+  ]
 
-foreach header: check_headers
-  assert(cc.has_header(header[1], args: cups_cflags), 'CUPS headers not found: ' + header[1])
-endforeach
+  foreach header: check_headers
+    assert(cc.has_header(header[1], args: cups_cflags), 'CUPS headers not found: ' + header[1])
+  endforeach
 
-config_h.set10('HAVE_CUPS_HTTPCONNECT2',
-               cc.has_function('httpConnect2', dependencies: cups_dep),
-               description: 'Define if httpConnect2() is available in CUPS')
+  config_h.set10('HAVE_CUPS_HTTPCONNECT2',
+                 cc.has_function('httpConnect2', dependencies: cups_dep),
+                 description: 'Define if httpConnect2() is available in CUPS')
+endif
+config_h.set('HAVE_CUPS', enable_cups,
+             description: 'Defined if cups support is enabled')
 
 # IBus support
 enable_ibus = get_option('ibus')
@@ -314,6 +320,7 @@ summary({
 }, section: 'Dependencies')
 
 summary({
+  'Cups': enable_cups,
   'IBus': enable_ibus,
   'Kerberos': enable_kerberos,
   'Snap': enable_snap,
diff --git a/meson_options.txt b/meson_options.txt
index bd5570fb4..f982b7b4e 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -1,3 +1,4 @@
+option('cups', type: 'boolean', value: true, description: 'build with cups support')
 option('documentation', type: 'boolean', value: false, description: 'build documentation')
 option('ibus', type: 'boolean', value: true, description: 'build with IBus support')
 option('kerberos', type: 'boolean', value: true, description: 'build with kerberos support')
diff --git a/panels/meson.build b/panels/meson.build
index 75941edcb..652c45f11 100644
--- a/panels/meson.build
+++ b/panels/meson.build
@@ -19,7 +19,6 @@ panels = [
   'notifications',
   'online-accounts',
   'power',
-  'printers',
   'region',
   'removable-media',
   'search',
@@ -31,6 +30,10 @@ panels = [
  'wwan',
 ]
 
+if enable_cups
+  panels += ['printers']
+endif
+
 if host_is_linux
   panels += ['network']
 endif
diff --git a/shell/cc-panel-loader.c b/shell/cc-panel-loader.c
index 66c513e18..a09b5566c 100644
--- a/shell/cc-panel-loader.c
+++ b/shell/cc-panel-loader.c
@@ -51,7 +51,9 @@ extern GType cc_wifi_panel_get_type (void);
 extern GType cc_notifications_panel_get_type (void);
 extern GType cc_goa_panel_get_type (void);
 extern GType cc_power_panel_get_type (void);
+#ifdef HAVE_CUPS
 extern GType cc_printers_panel_get_type (void);
+#endif /* HAVE_CUPS */
 extern GType cc_region_panel_get_type (void);
 extern GType cc_removable_media_panel_get_type (void);
 extern GType cc_search_panel_get_type (void);
@@ -122,7 +124,9 @@ static CcPanelLoaderVtable default_panels[] =
   PANEL_TYPE("notifications",    cc_notifications_panel_get_type,        NULL),
   PANEL_TYPE("online-accounts",  cc_goa_panel_get_type,                  NULL),
   PANEL_TYPE("power",            cc_power_panel_get_type,                NULL),
+#ifdef HAVE_CUPS
   PANEL_TYPE("printers",         cc_printers_panel_get_type,             NULL),
+#endif
   PANEL_TYPE("region",           cc_region_panel_get_type,               NULL),
   PANEL_TYPE("removable-media",  cc_removable_media_panel_get_type,      NULL),
   PANEL_TYPE("search",           cc_search_panel_get_type,               NULL),
diff --git a/tests/printers/meson.build b/tests/printers/meson.build
index 60f144fa6..d36c0a4ff 100644
--- a/tests/printers/meson.build
+++ b/tests/printers/meson.build
@@ -1,22 +1,22 @@
+if enable_cups
+  test_units = [
+    #'test-canonicalization',
+    'test-shift'
+  ]
 
-test_units = [
-  #'test-canonicalization',
-  'test-shift'
-]
+  includes = [top_inc, include_directories('../../panels/printers')]
+  cflags = '-DTEST_SRCDIR="@0@"'.format(meson.current_source_dir())
 
-includes = [top_inc, include_directories('../../panels/printers')]
-cflags = '-DTEST_SRCDIR="@0@"'.format(meson.current_source_dir())
-
-foreach unit: test_units
-  exe = executable(
-                    unit,
-           [unit + '.c'],
-    include_directories : includes,
-           dependencies : common_deps,
-              link_with : [printers_panel_lib],
-                 c_args : cflags
-  )
-
-  test(unit, exe)
-endforeach
+  foreach unit: test_units
+    exe = executable(
+                      unit,
+             [unit + '.c'],
+      include_directories : includes,
+             dependencies : common_deps,
+                link_with : [printers_panel_lib],
+                   c_args : cflags
+    )
 
+    test(unit, exe)
+  endforeach
+endif
-- 
2.35.1

