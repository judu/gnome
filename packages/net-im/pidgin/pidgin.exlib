# Copyright 2009, 2010 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require option-renames [ renames=[ 'ssl providers:gnutls' ] ]
require python [ has_bin=true multibuild=false blacklist=2 with_opt=true ]
require gtk-icon-cache

export_exlib_phases src_prepare src_install

SUMMARY="A multi-protocol, multi-platform Instant Messaging client."
DESCRIPTION="
Pidgin is an easy to use and free chat client used by millions. Connect
to Google Talk and more chat networks all at once.

Supported chat networks:

  * Bonjour
  * Gadu-Gadu
  * Google Talk
  * Groupwise
  * IRC
  * SILC
  * SIMPLE
  * Sametime
  * XMPP
  * Zephyr
"
HOMEPAGE="https://pidgin.im/"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.bz2"

REMOTE_IDS="sourceforge:pidgin freshcode:pidgin"

UPSTREAM_DOCUMENTATION="https://pidgin.im/development/faq/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    avahi     [[ description = [ Needed for the Bonjour protocol ] ]]
    dbus
    eds
    gstreamer [[ description = [ Needed for sound in the GTK+ client ] ]]
    gtk       [[ description = [ Enable the GTK+ 'pidgin' client ] ]]
    ncurses   [[ description = [ Enable the ncurses 'finch' client ] ]]
    networkmanager
    perl      [[ description = [ Support for plugins written in perl ] ]]
    python
    spell
    startup-notification
    tcl       [[ description = [ Support plugins written in tcl/tk ] ]]
    tk
    vv        [[ description = [ Enable voice and video support ] ]]

    (
        eds
        gstreamer
        spell
        startup-notification
    ) [[ requires = gtk ]]

    dbus [[ requires = python ]]
    networkmanager [[ requires = dbus ]]
    tcl [[ requires = tk ]]
    tk [[ requires = tcl ]]
    vv [[ requires = gstreamer ]]

    ( providers:
        gnutls
        nss
    ) [[ number-selected = exactly-one ]]

    ( linguas:
        af am ar ar_SA as ast az be@latin bg bn bn_IN br brx bs ca ca@valencia cs da de dz el en_AU
        en_CA en_GB eo es_AR es et eu fa fi fr ga gl gu he hi hr hu id it ja ka kk km kn ko ks ku ku_IQ
        lt lv mai mhr mk ml mn mr ms_MY my_MM nb ne nl nn oc or pa pl ps pt_BR pt ro ru sd si sk sl
        sq sr sr@latin sv sw ta te th tr tt uk ur uz vi xh zh_CN zh_HK zh_TW
    )
"


DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-perl/XML-Parser
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
        gtk? ( x11-proto/xorgproto )
    build+run:
        dev-libs/glib:2[>=2.26.0]
        net-libs/cyrus-sasl
        dev-libs/libxml2:2.0
        net-dns/libidn
        sys-libs/zlib
        avahi? ( net-dns/avahi )
        dbus? (
            dev-libs/dbus-glib:1[>=0.60]
            dev-python/dbus-python[python_abis:*(-)?]
            sys-apps/dbus
        )
        eds? ( gnome-desktop/evolution-data-server:1.2[>=3.6] )
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
            media-plugins/gst-plugins-good:1.0
        )
        gtk? (
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:2[>=2.10.0]
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libXScrnSaver
            x11-libs/pango[>=1.4.0]
        )
        networkmanager? ( net-apps/NetworkManager[>=0.5.0] )
        ncurses? (
            dev-libs/libgnt[>=2.14.0]
            sys-libs/ncurses
        )
        perl? ( dev-lang/perl:= )
        providers:gnutls? ( dev-libs/gnutls[>=2.10.0] )
        providers:nss? (
            dev-libs/nspr
            dev-libs/nss
        )
        spell? ( app-spell/gtkspell:2[>=2.0.2] )
        startup-notification? ( x11-libs/startup-notification[>=0.5] )
        tcl? ( dev-lang/tcl )
        tk? ( dev-lang/tk )
        vv? ( net-im/farstream:0.2[>=0.2.7] )
    suggestion:
        gnome-desktop/gsettings-desktop-schemas [[ description = [ GNOME 3 proxy settings support ] ]]
        gnome-platform/GConf:2 [[ description = [ GNOME 2 proxy settings support ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    "--with-ncurses-headers=/usr/$(exhost --target)/include/ncursesw"
    --disable-fuzzing
    # until we get exheres for these:
    '--disable-meanwhile'
    '--enable-idn'
    '--enable-nls'
)
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    'gstreamer --with-gstreamer=1.0'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'avahi'
    'dbus'
    'gstreamer'
    'gtk gtkui'
    'gtk screensaver'
    'ncurses consoleui'
    'networkmanager nm'
    'perl'
    'providers:gnutls gnutls yes'
    'providers:nss nss yes'
    'spell gtkspell'
    'startup-notification'
    'tcl'
    'tk'
    'vv'
    'vv farstream'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'gtk x'
    'perl perl-lib vendor'
    "python python ${PYTHON}"
    "tcl tclconfig /usr/$(exhost --target)/lib"
    "tk tkconfig /usr/$(exhost --target)/lib"
)

pidgin_src_prepare() {
    default

    # fix shebang, these are only installed if 'python' and 'dbus' are set
    edo sed -i -e "s|^#!.*python.*|#!${PYTHON}|g" \
        libpurple/purple-remote \
        libpurple/purple-url-handler

    edo intltoolize --force --automake

    edo sed -e "s/pkg-config/$(exhost --tool-prefix)&/" \
            -i configure
}

pidgin_src_install() {
    default

    if ! optionq gtk && ! optionq ncurses; then
        edo find "${IMAGE}" -type d -empty -delete
    fi
}

